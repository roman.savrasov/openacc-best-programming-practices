# OpenACC - Best Programming Practices

Für den Kurs OpenACC der TH Bingen durfte ich Kapitel 6 Best Programming Practices, aus dem Buch OpenACC for Programmers: Concepts and Strategies (ISBN-13: 978-0134694283), vorstellen durfte, habe ich in dieses Repo meine Präsentation, Zusammenfassung sowie meine Notizen gestellt.

### OpenACC

OpenACC ist ein Programming Standard, mit dem man mittels Compiler Direktiven C, C++ und Fortran Code einfach parallelisieren kann, ohne richtigen Code schreiben zu müssen.
Mehr Infos zu OpenACC unter <https://en.wikipedia.org/wiki/OpenACC>.